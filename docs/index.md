# cwilson.io

Figured I might as well start to make a collection of posts, guides, technical documentation, and other nonsense I've done across my career. Half as a way to document it for myself, half as a way to consolidate my experience for recruiters or other interested parties.

## Blog

[https://cwilson.io/blog](blog/index.md) contains posts I've made, from musings and thoughts to technical implementation guides.  More for my own therapeutic benefit than for anyone to actually read.

## Resume

Eventually a current copy of my personal resume/CV will be available here.  I'll probably write a post about how I made it, using a lovely Markdown/LaTeX project to generate the end-files like `pdf` and `rtf`.
