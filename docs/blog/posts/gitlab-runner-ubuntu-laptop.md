---
authors:
  - "cwilson"
categories:
  - Guide
  - Tech
  - Software
  - Markdown
  - Blog
date:
  created: 2023-11-22
---
# GitLab Runner on an old Ubuntu Laptop

## Why

I have an idle laptop and the need to automate some code builds from GitLab. I cut my teeth on GitLab in the Federal space, so it's my go-to; I've yet to find a need to use GitHub Actions, and these days I'm looking for something even more modern than GitLab to play with. OneDev sounds promising!

## How

I have an existing set of Ansible playbooks I've been using to manage the infrastructure and configuration of the machines at home.  The inventory is mapped out already, and using the moniker **Styrene**, which I'll get into at some other point.

## Ansible Overview

I already have the OS (Ubuntu 22.04) relatively configured, so I'll start there with a playbook that targets the laptop with the standardized Ubuntu role, followed by the (new) GitLab Runner role. I'm going to start by setting up a Shell Runner, haven't decided how I want to tackle runners from a conceptual level.  For now, with that decision unmade, I'll stick to Old Faithful.

``` title="basic tree of ansible dir"
.
├── LICENSE
├── README.md
├── ansible.cfg
├── inventory
│   └── styrene
├── playbooks
│   └── gitlab-runner.yaml
├── requirements.in
├── requirements.txt
├── roles
│   ├── k3s
│   │   ├── control
│   │   ├── download
│   │   ├── node
│   │   └── post
│   ├── kasm-server
│   ├── styrene
│   │   ├── gpu
│   │   └── ubuntu
│   └── truenas
└── templates
    ├── k3s-uninstall.sh
    ├── netplan-config.j2
    └── rc.local.j2
```

### GitLab Runner (Ubuntu) Playbook

To start, the inventory is already set up and ready to roll, we'll be starting with a known user and proper SSH-key auth, although we do play a little fast and loose with passwordless `sudo` escalation.

```yaml title="Playbook"
- name: "Deploy Improvised GitLab Runner (Shell)"
  hosts: asus-laptop
  become: true
  vars_files:
    - "vars/gitlab-runner.yaml"
  roles:
    - styrene/ubuntu
  # - styrene/gitlab-runner
```

The `styrene/ubuntu` role will wrap up with the user `styrene` being present, with the proper admin SSH key present for logins to that user. `root` user logins via SSH are disabled entirely, and `sudo` access is passwordless, allowing Ansible fairly straightforward access across the system(s).

I'm sure I'll make some ramblings on a new, better system than this one, but at least it's a key that I keep managed centrally until I get around to a full PKI solution like Vault.  

Anyway.

The `styrene/gitlab-runner` role is commented out until I lazily create the boilerplate with `ansible-galaxy`:

```bash title="Use ansible-galaxy to initialize a new role under styrene"
ansible-galaxy role init roles/styrene/gitlab-runner
```

``` title="Great Success"
- Role roles/styrene/gitlab-runner was created successfully
```

### GitLab Runner Variables

Here's how I'm structuring the variables file. Get your own token, freeloader.

```yaml title="Vars File Contents"
---
# vars file for roles/homelab/gitlab-runner
gitlab_runner_coordinator_url: "https://gitlab.com"
gitlab_runner_registration_token: REDACTED
gitlab_runners:
  - name: "Simple Shell Runner"
    executor: shell
    tags:
      - linux-shell
      - ubuntu
      - shell
```

### Role Install Tasks

Nobody starts from scratch, so big thanks and shoutout to `AUA` [where I got the starting point for this role tasking](https://anunknownalias.net/posts/gitlab-runner-setup/).

Unfortunately, this method of installing the runner is now deprecated as of GitLab 16.0...leading me further down the path of re-evaluation.  The new token system sounds interesting, and might make for the subject of another post, where I update this alongside a proper Kubernetes deployment of the GitLab Runner(s)...

So for now we'll treat this as a one-off and just install/register via CLI.

```yaml title="GitLab Runner Role tasks/main.yaml"
---
# tasks file for roles/styrene/gitlab-runner
- name: Install GitLab Runner (deb)
  shell: |
    curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
    dpkg -i gitlab-runner_amd64.deb
  args:
    creates: /usr/bin/gitlab-runner
- name: Register GitLab Runner (Shell)
  command: |
    gitlab-runner register \
    --non-interactive \
    --url "{{ gitlab_runner_coordinator_url }}" \
    --registration-token "{{ gitlab_runner_registration_token }}" \
    --executor "shell" \
    --description "{{ item.name }}" \
    --tag-list "{{ item.tags|join(',') }}" \
    --run-untagged="false" \
    --locked="false" \
    --access-level="ref_protected"
  loop: "{{ gitlab_runners }}"
```

### Sanity Check via GitLab Project's UI

Checking the project's Runner's Page on GitLab, we see a wild runner appear:

![Available Runners Page on GitLab](images/assigned-project-runners.png)

Our tags are also properly applied, and this Runner should pick up and execute any jobs with the `shell`, `linux-shell`, or `ubuntu` tags.
