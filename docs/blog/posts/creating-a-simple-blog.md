---
authors:
  - "cwilson"
categories:
  - Guide
  - Tech
  - Software
  - Markdown
  - Blog
date:
  created: 2023-11-21
  updated: 2023-11-22
---
# Creating a Simple Blog

Long story short, needed a simple blog to host technical guides and various posts. I'm going to start documenting my work for my own sanity and for posterity, and this is a slightly more digestible than pure technical documentation.

I've used [MKDocs](https://www.mkdocs.org) in the past for technical documentation, a lovely little Python project that builds a static site out of a directory of standard Markdown files; nice and simple, and I'm a die-hard proponent of plain-text where possible.  

MKDocs has a large community of themes, plugins, and frameworks. I'm going to use a tried-and-true option, [MKDocs-Material](https://squidfunk.github.io/mkdocs-material/) as the underlying framework.  MKDocs-Material has a built-in `blog` plugin to handle the tedious logistics of things like organizing posts, author information, or a tabe of contents.  All I need to focus on is spitting out my "Post" in a single Markdown file in the proper directory.

## Prep Work

I've installed Python 3.11 (as of writing) via `brew` onto my M1 MacBook Pro running MacOS Sonoma.

### Virtual Environment

First steps will be to isolate our dependencies using a Python Virtual Environment.  Let's make a project directory `simple-blog`, and then create our `venv` so we can start installing packages.

```bash
mkdir -p simple-blog
cd simple-blog/
which python3
```

```bash title="Example Output (No venv)"
/opt/homebrew/bin/python3
```

```bash
python3 -m venv venv
source venv/bin/activate
which python3
```

```bash title="Example Output (With venv)"
/Users/chriswilson/cwilson/simple-blog/venv/bin/python3
```

!!! success

### Git Setup

We can see our Python environment is now contained within the `venv` directory.  Let's go ahead and initialize `git` now and add the `venv` path to our `.gitignore` so that it isn't picked up and tracked by version control:

```bash
git init --initial-branch=main
```

```bash title="Example Output"
Initialized empty Git repository in /Users/chriswilson/cwilson/simple-blog/.git/
```

We can see that `git` is already looking at the `venv/` directory in its untracked files:

```bash
git status
```

``` title="Example Output" hl_lines="10"
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        README.md
        docs/
        mkdocs.yml
        venv/

nothing added to commit but untracked files present (use "git add" to track)
```

We want to ignore that folder as it's going to contain all of our packages, dependencies, and binary links for development, but nothing pertaining to the source code or plaintext posts themselves.  `.gitignore` files provide a robust interface for configuring what `git` will ignore (crazy, given the name, right?).

We'll just add our `venv/` folder line as a pattern to the file.  Since it's the only one, we'll just create it with a one-liner:

```bash
echo -n "venv/" > .gitignore
```

Now let's look at what `git` intends to do by running another `status` check after adding our ignore pattern:

```bash
git status
```

``` title="Example Output after adding .gitignore"
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        .gitignore
        README.md
        docs/
        mkdocs.yml

nothing added to commit but untracked files present (use "git add" to track)
```

!!! success
    Fantastic, our `venv/` directory is no longer picked up. I encourage you to
    keep track of your repo in `git` independently after this.

Now that we have our Virtual Environment set up, let's use it to install the Python package `mkdocs-material`. This has dependencies including MKDocs itself, and will take care of most of our requirements.  I also want the RSS plugin for MKDocs, so I'll throw that in as well:

```bash title="Install packages with pip3"
pip3 install mkdocs-material mkdocs-rss-plugin
```

### MKDocs

Now let's verify that we have the `mkdocs` binary, and that it lives in our `venv/` folder:

```bash
which mkdocs
```

```bash title="Expected Output"
/Users/chriswilson/cwilson/simple-blog/venv/bin/mkdocs
```

Create our project!

```bash title="Create our new MKDocs project"
mkdocs new
```

!!! success
    Great, everything looks properly set up and installed. Let's move on to creating a Makefile to streamline how we build and serve the site during development.

## Makefile

Pretty soon I may want to hook this up to some CI/CD tooling to automate some of the processes.  Until then, I'll use a `Makefile` to ensure consistent executions.  That should also provide a convenient interface for the CI/CD tooling in the future as well, and will ensure that both automated and manual operations will perform as identically as they can.  Using an OCI Container as an execution environment would narrow that gap to near-nothing, and we may tackle that later on.

Let's go ahead and look at the command I need to run in order to start the blog site with `mkdocs`:

```bash
mkdocs serve
```

``` title="Expected Output"
INFO    -  Building documentation...
INFO    -  Cleaning site directory
INFO    -  Documentation built in 0.15 seconds
INFO    -  [14:06:47] Watching paths for changes: 'docs', 'mkdocs.yml'
INFO    -  [14:06:47] Serving on http://127.0.0.1:8000/
```

That command will build our site (Markdown -> Static Site), then serve it with a small development webserver bound to `127.0.0.1` on port `8000`.  

We should be able to hit `http://localhost:8000` to see this hosted, and we'll get to that in a bit.  For now, let's just worry about getting our makefile up and ready, so that if we want to switch from using our local-environment `mkdocs` and Python to using a container, we can make that change in a single place.

`Ctrl+C` to kill the running development server (if you haven't already), then let's create our Makefile:

```bash
touch Makefile
```

I'll add some copy-pasted values massaged from another project, but Makefiles are ubiquitous and have very robust documentation available online.  Think of it as "intelligent aliasing", where you run `make <command>`, and `<command>` is defined in the `Makefile` as the execution of one-or-more steps, including logic and conditionals.  Our file will be very simple, but for complex codebases, compilation steps can get ludicrously large and complex.

The following Makefile content should give us a consistent way to `build`, and `serve` commands.  These can be as simple as giving us the ability to type `make serve` instead of `mkdocs serve --clean --dev-addr 0.0.0.0:8080`, or as complex as having `make build` not only run a `mkdocs build` but also then build an OCI image as a deployment package.

```Makefile title="Example Makefile"
.PHONY: build serve
build:
  mkdocs build
serve:
  mkdocs serve --clean --dev-addr 0.0.0.0:8080
```

!!! warning
    Makefiles require the `TAB` character! You will see separator errors if you have spaces instead.

```bash title="Run the build Command with Make"
make build
```

```title="Example Build Output"
mkdocs build
INFO    -  Cleaning site directory
INFO    -  Building documentation to directory: /Users/chriswilson/cwilson/simple-blog/site
INFO    -  Documentation built in 0.16 seconds
```

```bash title="Run the serve Command with Make"
make serve
```

``` title="Example Serve Output (Live)"
mkdocs serve --clean --dev-addr 0.0.0.0:8080
WARNING -  Config value 'dev_addr': The use of the IP address '0.0.0.0' suggests a production environment or the use
           of a proxy to connect to the MkDocs server. However, the MkDocs' server is intended for local development
           purposes only. Please use a third party production-ready server instead.
INFO    -  Building documentation...
INFO    -  Cleaning site directory
INFO    -  Building documentation to directory: /var/folders/y1/2sss9w655bg33vx152pzc6dw0000gn/T/mkdocs_w292b1oy
INFO    -  Documentation built in 0.15 seconds
INFO    -  [14:25:10] Watching paths for changes: 'docs', 'mkdocs.yml'
INFO    -  [14:25:10] Serving on http://0.0.0.0:8080/
```

As we can see, our `make` commands are just delegating and running other commands to complete their tasking.  For now, that is plenty, and it keeps our build/serve steps consistent (won't pick the wrong port on accident once).

## MKDocs Config

For MKDocs specifically, our `mkdocs.yaml` file contains all of the main configuration.  This is also true for `mkdocs-material`, so a single file manages all of the MKDocs-related details.  I've already played around and chosen two themes, one for "light" and another for "dark" mode, along with their respective colors.

The structure I'm using includes references to the RSS plugin, currently commented out until I get there:

```yaml title="Sample mkdocs.yaml Configuration File" hl_lines="1 3 16 17"
site_name: Simple Blog
plugins:
  - blog:
      blog_toc: true
      post_date_format: full
      post_url_date_format: yyyy/MMM/dd
  - search
  # - rss:
  #     match_path: blog/posts/.*
  #     date_from_meta:
  #       as_creation: date
  #     categories:
  #       - categories
  #       - tags
theme:
  name: material
  palette:
    - scheme: default
      toggle:
        icon: material/brightness-7
        name: Switch to light mode
      primary: blue-grey
      accent: deep-purple
    - scheme: slate
      toggle:
        icon: material/brightness-3
        name: Switch to dark mode
      primary: black
      accent: deep-purple
```

The highlighted lines show the really impactful lines.  The `theme` section is what loads the `mkdocs-material` framework that we want, so that is set to `name: material`.  The `pallete` section can be reconfigured and customized, available options can be found on the [MKDocs Material Documentation](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/).

---

## First Look

Let's take a peek at how things are coming along!  I'm building the plane while it's in the air, so screenshots are in-development-examples, including the content of this post itself.

Let's fire up our development server:

```bash
make serve
```

This will automatically reload whenever file changes are detected, so we can edit and the development server will reload to show the new changes.

![First Look in a Browser](images/first-look.png)

We can see a lot of garbage and unrendered content, which I think is from some missing plugins.  I've used a few markdown syntax options without their required plugins, so I'll add those in and see.

The missing piece of the puzzle is going to involve the `markdown-extensions` block:

```yaml title="Snippet for mkdocs.yaml"
markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.superfences
```

Let's add that to our `mkdocs.yml` file and refresh the page:

![After Fixing Markdown Extensions in mkdocs.yml](images/fixed-markdown-extensions.png)

Much better! The code blocks now have proper highlighting for lines and syntax, while the admonitions render properly.  I'll need to fix some titles and add some polish for the theme to jive, but that's mostly it.  Now I should be able to begin working on the OCI image functionality.  I definitely want to perform the build and serve steps from the container, but also want to build a custom container to run at home and serve this blog at `https://blog.cwilson.io`.  I already own the domains required, and I'll use a CloudFlare Tunnel to provide the TLS termination while only having to serve a static site via HTTP from my container.

The first step will be moving the Makefile to use the official `squidfunk` OCI image, removing the dependency on Python/MKDocs/Venv.

It looks like the command `mkdocs serve ...` can be replaced with `docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material serve ...`. Let's try it out...

```bash title="MKDocs Serve from OCI Container"
docker run --rm -it \
  -p 8080:8080 \
  -v ${PWD}:/docs \
  squidfunk/mkdocs-material serve --dev-addr 0.0.0.0:8080
```

I see the container logs refreshing and spitting out:

```txt
INFO    -  [20:04:41] Browser connected: http://localhost:8080/blog/
```

So I'm going to add that to our Makefile and verify! The Makefile should look like this now:

```Makefile title="New Makefile with OCI Image"
.PHONY: build serve
build:
  docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material build
serve:
  docker run --rm -it -p 8080:8080 -v ${PWD}:/docs squidfunk/mkdocs-material serve --dev-addr 0.0.0.0:8080
```

```bash title="Verify Build"
make build
```

``` title="Example Build Output"
docker run --rm -it -v /Users/chriswilson/cwilson/simple-blog:/docs squidfunk/mkdocs-material build
INFO    -  Cleaning site directory
INFO    -  Building documentation to directory: /docs/site
INFO    -  Documentation built in 0.33 seconds
```

```bash title="Verify Serve"
make serve
```

``` title="Example Build Output"
docker run --rm -it -p 8080:8080 -v /Users/chriswilson/cwilson/simple-blog:/docs squidfunk/mkdocs-material serve --dev-addr 0.0.0.0:8080
WARNING -  Config value 'dev_addr': The use of the IP address '0.0.0.0' suggests a production environment or the use
           of a proxy to connect to the MkDocs server. However, the MkDocs' server is intended for local development
           purposes only. Please use a third party production-ready server instead.
INFO    -  Building documentation...
INFO    -  Cleaning site directory
INFO    -  Documentation built in 0.26 seconds
INFO    -  [20:08:47] Watching paths for changes: 'docs', 'mkdocs.yml'
INFO    -  [20:08:47] Serving on http://0.0.0.0:8080/
```

!!! success
    Looks Good!

## Containerize

We have our `build` step, but building the documentation from source is something that the `serve` function will do continuously.  That's great during development, but when we want to host it somewhere accessible remotely, we'll need some kind of web server even for a static site.  To keep it small, I'll only be using the `squidfunk` image to *build* the static site files, but a different, very tiny image to *serve* them.

I'm going to shamelessly use this clever little `httpd` minimalist image that I came across on [this blog](https://lipanski.com/posts/smallest-docker-image-static-website).

The default `Dockerfile` contents aren't quite what we need though:

```Dockerfile title="Original Lipanski-based Dockerfile"
FROM lipanski/docker-static-website:latest

COPY . .

CMD ["/busybox", "httpd", "-f", "-v", "-p", "3000", "-c", "httpd.conf"]
```

I'd rather not use `latest` just as a matter of course, so after looking at the [Dockerhub page](https://hub.docker.com/r/lipanski/docker-static-website), I settled on the tag `2.2.0`.  We'll also want to copy the output of only the `site/` folder where our static site files are placed after a build.  We also want to use the same port `8080` that we've been using, rather than the `3000` specified above. Let's also fully qualify the image URL to include `docker.io`, rather than using short names.

```Dockerfile title="Tweaked Lipanski-based Dockerfile"
FROM docker.io/lipanski/docker-static-website:2.2.0

COPY site/ .

CMD ["/busybox", "httpd", "-f", "-v", "-p", "8080", "-c", "httpd.conf"]
```

Being on an M1 Mac, I'll need to take care to specify the target platform, `amd64`, otherwise my native `arm64` will be used.

```bash title="Build our Deployment Image"
docker build --platform=linux/amd64 -t test-simple-blog .
```

``` title="Build Output"
[+] Building 0.7s (8/8) FINISHED                                                                                        docker:desktop-linux
 => [internal] load build definition from Dockerfile                                                                                    0.0s
 => => transferring dockerfile: 204B                                                                                                    0.0s
 => [internal] load .dockerignore                                                                                                       0.0s
 => => transferring context: 2B                                                                                                         0.0s
 => [internal] load metadata for docker.io/lipanski/docker-static-website:2.2.0                                                         0.6s
 => [auth] lipanski/docker-static-website:pull token for registry-1.docker.io                                                           0.0s
 => [internal] load build context                                                                                                       0.0s
 => => transferring context: 4.65kB                                                                                                     0.0s
 => CACHED [1/2] FROM docker.io/lipanski/docker-static-website:2.2.0@sha256:5beabd0caa1acb1d393d1bdac0bc57d7de9316c644fc48d41fa9cf6af2  0.0s
 => => resolve docker.io/lipanski/docker-static-website:2.2.0@sha256:5beabd0caa1acb1d393d1bdac0bc57d7de9316c644fc48d41fa9cf6af24bca9e   0.0s
 => [2/2] COPY site/* .                                                                                                                 0.0s
 => exporting to image                                                                                                                  0.0s
 => => exporting layers                                                                                                                 0.0s
 => => writing image sha256:b006c9d8d20445c29e52c29117cb9bda2303f937253b99fef49111757dbb0a11                                            0.0s
 => => naming to docker.io/library/test-simple-blog                                                                                     0.0s
```

Now we can run it locally as a sanity-check:

```bash title="Run Newly Built Blog Image"
docker run -d -p 8080:8080 test-simple-blog
```

``` title="Expected Output"
03b418d396009349fa233931c81fd26867b6c98f7d7ff6597eaaf959d63dccec
```

`http://localhost:8080` now seems to work correctly as well!

I need to give it a real home, so I've created a repository for the image on Dockerhub:

`cwilson613/simple-blog`

### .gitignore site/

I'm also going to add the `site/` and `.cache/` directories to the patterns in `.gitignore` so that I don't check-in any build artifacts.

```bash title="One Liner to Append 'site/' to .gitignore"
echo -n "site/" >> .gitignore
```

``` title=".gitignore"
venv/
site/
.cache/
```

Let's go ahead and add that to our Makefile's `build` command to build and push the image:

```Makefile title="Makefile With Expanded Build Options"
GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
IDENTIFIER = $(CLASS_NAME)-$(GIT_BRANCH)-$(ORG)
IMAGE = "docker.io/cwilson613/simple-blog"

.PHONY: build serve
build:
  docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material build
  docker build -t ${IMAGE}:${GIT_BRANCH} .
  docker tag ${IMAGE}:${GIT_BRANCH} ${IMAGE}:latest
  docker push ${IMAGE}:${GIT_BRANCH}
  docker push ${IMAGE}:latest
serve:
  docker run --rm -it -p 8080:8080 -v ${PWD}:/docs squidfunk/mkdocs-material serve --dev-addr 0.0.0.0:8080
```

In the file we'll use git to grab the current branch (or tag) as our oci image tag.  We build the image, then tag it as `latest`, before pushing both the versioned and latest tagged images.

Let's fire it off!

!!! warning
    This absolutely glosses over logging into the DockerHub registry. That is outside the scope here, but worth learning. I'll leave myself a TODO here in case I ever write something up on that topic, I'll link it here.

```bash title="Run the New Build Process"
make build
```

``` title="Build Output"
docker run --rm -it -v /Users/chriswilson/cwilson/simple-blog:/docs squidfunk/mkdocs-material build
INFO    -  Cleaning site directory
INFO    -  Building documentation to directory: /docs/site
INFO    -  Documentation built in 0.37 seconds
docker build -t "docker.io/cwilson613/simple-blog":main .
[+] Building 0.9s (8/8) FINISHED                                                               docker:desktop-linux
 => [internal] load .dockerignore                                                                              0.0s
 => => transferring context: 2B                                                                                0.0s
 => [internal] load build definition from Dockerfile                                                           0.0s
 => => transferring dockerfile: 205B                                                                           0.0s
 => [internal] load metadata for docker.io/lipanski/docker-static-website:2.2.0                                0.7s
 => [auth] lipanski/docker-static-website:pull token for registry-1.docker.io                                  0.0s
 => [internal] load build context                                                                              0.0s
 => => transferring context: 3.70MB                                                                            0.0s
 => CACHED [1/2] FROM docker.io/lipanski/docker-static-website:2.2.0@sha256:5beabd0caa1acb1d393d1bdac0bc57d7d  0.0s
 => [2/2] COPY site/ .                                                                                         0.0s
 => exporting to image                                                                                         0.0s
 => => exporting layers                                                                                        0.0s
 => => writing image sha256:751c8eeb587196812d0511b873e8c2395edfca70b9d17914eeefd1ee4568352f                   0.0s
 => => naming to docker.io/cwilson613/simple-blog:main                                                         0.0s
docker tag "docker.io/cwilson613/simple-blog":main "docker.io/cwilson613/simple-blog":latest
docker push "docker.io/cwilson613/simple-blog":main
The push refers to repository [docker.io/cwilson613/simple-blog]
8c6e4d63ea8b: Pushed 
32ae32e7187c: Mounted from lipanski/docker-static-website 
aa2dcf717abd: Mounted from lipanski/docker-static-website 
2c8b629ea3de: Mounted from lipanski/docker-static-website 
6969a86cee0b: Mounted from lipanski/docker-static-website 
main: digest: sha256:7ad65ea5d090201df2898a7acd1249b2cab333895d2d36e0fc7f658e283edeea size: 1358
docker push "docker.io/cwilson613/simple-blog":latest
The push refers to repository [docker.io/cwilson613/simple-blog]
8c6e4d63ea8b: Layer already exists 
32ae32e7187c: Layer already exists 
aa2dcf717abd: Layer already exists 
2c8b629ea3de: Layer already exists 
6969a86cee0b: Layer already exists 
latest: digest: sha256:7ad65ea5d090201df2898a7acd1249b2cab333895d2d36e0fc7f658e283edeea size: 1358
```

As we can see here, I have initialized and pushed this to a personal git repo, which is setting the `GIT_BRANCH` variable (in this case, we see it resolve to just `main`).

After a `make build`, we will see two new tags pushed:

- `cwilson613/simple-blog:<git-branch>` (=> `cwilson613/simple-blog:main`)
- `cwilson613/simple-blog:latest`

Let's try running that image and verify it's functioning:

```bash title="Run the Pushed Image"
docker run -d -p 8080:8080 cwilson613/simple-blog:latest
```

``` title="Running in background"
9dce47338331682bb336ee6828cf2358733341f66a4279e0ab1942d69282187c
```

!!! warning "Note for users on `arm64`:"
    You may see a warning like the following:

    ```
    WARNING: The requested image's platform (linux/amd64) does not match the detected host platform (linux/arm64/v8) and no specific platform was requested
    9dce47338331682bb336ee6828cf2358733341f66a4279e0ab1942d69282187c
    ```

    Either add a `--platform linux/amd64` to the build command or feel free to ignore on MacOS.  You can also build an `arm64` image by adding the `--platform` target flag in the Makefile for the `build` stage to ensure Docker creates images for `arm64` natively.

Let's add a `make run` command to pull and run the `:latest` tag to make our lives a little easier.

```Makefile
GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
IDENTIFIER = $(CLASS_NAME)-$(GIT_BRANCH)-$(ORG)
IMAGE = "docker.io/cwilson613/simple-blog"

.PHONY: build serve
build:
  docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material build
  docker build -t ${IMAGE}:${GIT_BRANCH} .
  docker tag ${IMAGE}:${GIT_BRANCH} ${IMAGE}:latest
  docker push ${IMAGE}:${GIT_BRANCH}
  docker push ${IMAGE}:latest
serve:
  docker run --rm -it -p 8080:8080 -v ${PWD}:/docs squidfunk/mkdocs-material serve --dev-addr 0.0.0.0:8080
run:
  docker kill simple-blog || echo "Not running"
  docker run -d --name simple-blog -p 8080:8080 cwilson613/simple-blog:latest
```

## Kubernetes Deployment

It's not elegant, but it's functional.  The goal is to have Kubernetes managing this container, not running it on a local system via some OCI runtime - so after it pushes, we'll go ahead and just apply some raw Kubernetes manifests for the time being.

Below are the raw manifests that I am using, note that I use Traefik and Cert-Manager to handle certificates and routing, but thankfully as this is a static public site (regenerated per build), we don't need to consider storage or authentication.  Another assumption is that the primary namespace for my cluster is `core`, which is reflected in the included Middlewares.  I have cross-namespace resource resolution enabled for Traefik, as a consideration if you want to travel this route.

```yaml title="cwilson.namespace.yaml"
apiVersion: v1
kind: Namespace
metadata:
  name: cwilson
```

```yaml title="simple-blog.deployment.yaml"
apiVersion: apps/v1
kind: Deployment
metadata:
  name: simple-blog
  namespace: cwilson
spec:
  replicas: 1
  selector:
    matchLabels:
      app: simple-blog
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: simple-blog
    spec:
      containers:
        - image: cwilson613/simple-blog:latest
          imagePullPolicy: Always
          name: simple-blog
          resources:
            limits:
              cpu: 100m
              memory: 512Mi
            requests:
              cpu: 100m
              memory: 512Mi
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
      restartPolicy: Always
      schedulerName: default-scheduler
```

```yaml title="simple-blog.service.yaml"
apiVersion: v1
kind: Service
metadata:
  name: simple-blog
  namespace: cwilson
spec:
  ports:
    - name: web
      port: 8080
      protocol: TCP
      targetPort: 8080
  selector:
    app: simple-blog
  type: ClusterIP
```

```yaml title="simple-blog.ingress.yaml"
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    cert-manager.io/cluster-issuer: styrene-letsencrypt-issuer
    traefik.ingress.kubernetes.io/enable: "true"
    traefik.ingress.kubernetes.io/router.entrypoints: web, websecure
    traefik.ingress.kubernetes.io/router.middlewares: core-https-redirect@kubernetescrd, core-cors@kubernetescrd
    traefik.ingress.kubernetes.io/router.tls: "true"
  name: simple-blog
  namespace: cwilson
spec:
  ingressClassName: traefik
  rules:
    - host: cwilson.io
      http:
        paths:
          - backend:
              service:
                name: simple-blog
                port:
                  number: 8080
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - cwilson.io
      secretName: simple-blog-public-tls
status:
  loadBalancer: {}
```

The redirect Middlewares:

```yaml title="Redirect Middleware(s)"
---
apiVersion: traefik.io/v1alpha1
kind: Middleware
metadata:
  name: cors
  namespace: core
spec:
  headers:
    accessControlAllowHeaders:
      - "*"
    accessControlAllowMethods:
      - GET
      - OPTIONS
      - PUT
    accessControlAllowOriginList:
      - "*"
    accessControlMaxAge: 100
    addVaryHeader: true
---
apiVersion: traefik.io/v1alpha1
kind: Middleware
metadata:
  name: https-redirect
  namespace: core
spec:
  redirectScheme:
    permanent: true
    scheme: https
```

I may create a seperate post and cross-link for a more robust deployment and release strategy to Kubernetes.

At this point I have this entire blog (starting with this meta post!) hosted here:

- Source Code: [https://gitlab.com/cwilson613/simple-blog](https://gitlab.com/cwilson613/simple-blog)
- [Live Link to cwilson.io](https://cwilson.io)
- [Live Link to Chris's Simple Blog](https://cwilson.io/blog)
