---
authors:
  - "cwilson"
categories:
  - Guide
  - Tech
  - Software
  - Blog
date:
  created: 2023-11-23
---
# Simple Automation of Image Builds using GitLab Runner

For the [Simple Blog](creating-a-simple-blog.md), we have a `makefile` and some steps for building the `Dockerfile` already.

Adding a `.gitlab-ci.yaml` file is the next step, the file that GitLab will use for defining the jobs in the pipeline.

The current GitLab Runner is running on an older Laptop, which I stood up in [GitLab Runner on Ubuntu Laptop](gitlab-runner-ubuntu-laptop.md), configured for Shell executor.

I am using `podman` instead of `docker` for the command line availability.

The GitLab project has CI/CD Variables pre-set for both `OCI_REGISTRY_USERNAME` and `OCI_REGISTRY_PASSWORD`.

The stages `build` and `package` will run back to back, due to their order and the `artifacts`/`dependencies` relationship between the jobs.

First the `site/` directory will be built using `mkdocs build` from within the `squidfunk` image, then passing that directory of static website files to the next job which builds the OCI image.

Just like in the `Makefile`, we will build an image, then tag it both as the `git` branch and `latest` tags to keep things versioned in a relatively sane way.

```yaml title=".gitlab-ci.yml"
variables:
  OCI_IMAGE_REGISTRY: "docker.io/cwilson613/simple-blog"
  OCI_IMAGE_TAG: "${OCI_IMAGE_REGISTRY}:${CI_COMMIT_REF_NAME}"
  OCI_IMAGE_LATEST: "${OCI_IMAGE_REGISTRY}:latest"
  MKDOCS_IMAGE: squidfunk/mkdocs-material:9

stages:
  - build
  - package

# Define a generic build template for generating the static site files

.docker-login: &docker-login
  tags:
    - shell
  before_script:
    - podman login -u "$OCI_REGISTRY_USERNAME" -p "$OCI_REGISTRY_PASSWORD" "$OCI_IMAGE_REGISTRY"

build:site:
  image:
    name: ${MKDOCS_IMAGE}
    entrypoint: [""]
  stage: build
  script:
    - mkdocs build
  artifacts:
    paths:
      - site/

package:oci:
  <<: *docker-login
  stage: package
  only:
    - tags
  script:
    - podman build -t $OCI_IMAGE_TAG .
    - podman tag $OCI_IMAGE_TAG $OCI_IMAGE_LATEST
    - podman login -u "$OCI_REGISTRY_USERNAME" -p "$OCI_REGISTRY_PASSWORD" $OCI_IMAGE_REGISTRY
    - podman push $OCI_IMAGE_TAG
    - podman push $OCI_IMAGE_LATEST
  dependencies:
    - build:site
```

It took a few iterations to get everything wired up correctly, but now we can see that the `build:site` job runs for every commit, and a `package:oci` runs only for `tagged` branches (like `0.0.7`):

![Functioning Pipeline](images/simple-blog-pipeline.png)
