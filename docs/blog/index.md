# Blog

I created this blog as a way to get things out of my head and into the world, for better or worse.  It's also more enjoyable to write in this format, so that's a plus - I'm not going to consistently do it if it's a chore.
