FROM docker.io/lipanski/docker-static-website:2.2.0

COPY site/ .

CMD ["/busybox", "httpd", "-f", "-v", "-p", "8080", "-c", "httpd.conf"]
