GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
IDENTIFIER = $(CLASS_NAME)-$(GIT_BRANCH)-$(ORG)
IMAGE = "docker.io/cwilson613/simple-blog"

.PHONY: build serve
build:
		docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material build
		docker build -t ${IMAGE}:${GIT_BRANCH} .
		docker tag ${IMAGE}:${GIT_BRANCH} ${IMAGE}:latest
		docker push ${IMAGE}:${GIT_BRANCH}
		docker push ${IMAGE}:latest
serve:
		docker run --rm -it -p 8080:8080 -v ${PWD}:/docs squidfunk/mkdocs-material serve --dev-addr 0.0.0.0:8080
run:
		docker kill simple-blog || echo "Not running"
		docker rm simple-blog || echo "Not found"
		docker run --platform linux/amd64 -d --name simple-blog -p 8080:8080 cwilson613/simple-blog:latest
